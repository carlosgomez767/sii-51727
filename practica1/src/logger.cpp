#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>

#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

using namespace std;

#define TAM 61

int main (){
	int estado_crear, estado_lectura,estado_cerrar,estado_unlink,estado_leer;
	char buff[TAM];
	//const char* fifo="/tmp/fifo";              //ruta del fifo

	//crear tubería
	estado_crear= mkfifo("/tmp/fifo",0777);
	if(estado_crear==-1){
		perror("error al crear el fifo");
		exit(1);
	}

	//apertura de la tubería en modo lectura
	estado_lectura=open("/tmp/fifo",O_RDONLY);     // si lo abre bien devuelve el nuevo DESCRIPTOR DE FICHERO.
	if(estado_lectura==-1){
		perror("error al abrir el fifo");
		exit(1);
	}

	while(1){
		estado_leer=read(estado_lectura,buff,sizeof(char)*TAM);
		if(estado_leer<=0){
			perror("error al leer el buffer");
			exit(2);
		}	
		write(1,buff,TAM*sizeof(char));   // 1 -- salida estándar
	}
	estado_unlink=unlink("/tmp/fifo");
	estado_cerrar=close(estado_lectura);
	if(estado_unlink==-1){
		perror("error al eliminar el nombre del sistema de ficheros");
		exit(1);
	}
	return 0;
}
